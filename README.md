# README #

README details the structure of the GuideScan development repository

# Cloning #

When cloning this directory to your machine and running the GuideScan software, please do so in locations with filepaths that do not contain spaces:

Correct Use: /Users/pereza1/Desktop

Incorrect Use: /Users/pereza1/Google\ Drive/

# Install #

### guidescan-crispr ###

guidescan-crispr contains the package scripts for GuideScan. One directory down contains the installation script for the package 
```
#!python

python setup.py build
python setup.py install

```
This directory also contains a subdirectory entitled trie which contains the C++ code for the trie data structure that GuideScan utilizes. Finally, this directory has a subdirectory entitled guidescan which contains the core scripts upon which the GuideScan package depends.

# Dependencies #

GuideScan has the following dependencies:

### Python Version ###
* python 2.7

### Non-Python Dependencies ###
* samtools version 1.3.1
* coreutils: specifically shuf
* rename
* easy_install 

for OSX users these dependencies can be installed via: 
`brew install samtools`,`brew install rename`

for .deb linux users these dependencies can be installed via:
`sudo apt-get install samtools rename`

for .rpm based linux users these dependencies can be installed via:
`sudo yum install samtools rename`

samtools can also be found here: http://www.htslib.org/download/. Unfortunately, versions of samtools <=1.2 are not compatible with GuideScan software.

### Python Dependencies ###
If you run setup.py these installations will be done for you
* biopython>=1.66
* pysam==0.8.3
* pyfaidx==0.4.7.1
* bx-python==0.7.3

If you desire to compute Rule Set 2 on-target cutting efficiency scores 
install sklearn==0.16.1 (must be installed by user, https://pypi.python.org/pypi/scikit-learn/0.16.1)